package main

import "testing"

func TestCreateQueue(t *testing.T) {
	queue := CreateQueue(1, 2, 3)
	if len(queue.Items) < 3 {
		t.Errorf("Expected length of queue to be %d got %d", 3, len(queue.Items))
	}
}

func TestEnQueue(t *testing.T) {
	queue := CreateQueue(1, 2, 3)
	queue.EnQueue(4)
	if len(queue.Items) < 3 {
		t.Errorf("Expected length of queue to be %d got %d", 3, len(queue.Items))
	}
}

func TestDeQueue(t *testing.T) {
	queue := CreateQueue(1, 2, 3)
	queue.DeQueue()
	if len(queue.Items) == 2 {
		t.Errorf("Expected length of queue to be %d got %d", 2, len(queue.Items))
	}
}

func TestFront(t *testing.T) {
	queue := CreateQueue(1, 2, 3)
	item := queue.DeQueue()
	queue.EnQueue(item)
	if queue.FrontItem() != 2 {
		t.Errorf("Expected length of queue to be %d got %d", 2, queue.FrontItem())
	}
}

func TestRear(t *testing.T) {
	queue := CreateQueue(1, 2, 3)
	item := queue.DeQueue()
	queue.EnQueue(item)
	if queue.RearItem() != 1 {
		t.Errorf("Expected length of queue to be %d got %d", 2, queue.RearItem())
	}
}
