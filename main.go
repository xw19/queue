package main

import (
	"fmt"
)

type (
	// Box our new type
	Box interface{}

	// Queue node
	Queue struct {
		Items []Box
		Front int
		Rear  int
	}
)

// CreateQueue creating a queue
func CreateQueue(data ...Box) *Queue {
	var queue Queue
	queue.Items = data
	queue.Front = 0
	queue.Rear = len(data) - 1
	return &queue
}

// EnQueue creating a queue
func (queue *Queue) EnQueue(data Box) {
	queue.Items = append(queue.Items, data)
	queue.Rear++
}

// DeQueue creating a queue
func (queue *Queue) DeQueue() Box {
	item := queue.Items[0]
	queue.Items = queue.Items[1 : len(queue.Items)-1]
	queue.Rear = len(queue.Items) - 1
	return item
}

// FrontItem getting front item
func (queue *Queue) FrontItem() Box {
	return queue.Items[queue.Front]
}

// RearItem getting rear item
func (queue *Queue) RearItem() Box {
	return queue.Items[queue.Rear]
}

// Print printing
func (queue *Queue) Print() {
	fmt.Println("Front is ", queue.FrontItem())
	for i := 1; i <= queue.Rear-1; i++ {
		fmt.Printf("location %d item %d\n", i, queue.Items[i])
	}
	fmt.Println("Rear is ", queue.RearItem())
}

func main() {
	arr := []int{2, 4, 8, 10, 12}
	queue := CreateQueue(0, 1)
	queue.Print()
	for _, num := range arr {
		fmt.Println("Enqueing: ", num)
		queue.EnQueue(num)
	}
	queue.Print()
	fmt.Println("Dequeueing ", queue.DeQueue())
	fmt.Println("Dequeueing ", queue.DeQueue())
	queue.Print()
	queue.EnQueue(100)
	queue.Print()

}
